﻿using UnityEngine;
using System.Collections;

public class SplashScreen : MonoBehaviour 
{
	public float displayTime, startTime;
	private float timer;
	public CanvasGroup fader;
	private float alpha;
	private bool fadeOut;

	private void Awake()
	{
		fadeOut = false;
		fader.alpha = 0;
		timer = 0;
	}

	private void Update()
	{
		if(!fadeOut)
		{
			if(RunTimer() > startTime)
			{
				FadeIn();
			}

			if(alpha > 1)
			{
				alpha = 1;
				fadeOut = true;
				timer = 0;
			}
		}
		else
		{
			if(RunTimer() > displayTime)
			{
				FadeOut();
			}

		}

		fader.alpha = alpha;
	}

	private void FadeIn()
	{
		alpha += 0.75f * Time.deltaTime;
	}

	private void FadeOut()
	{
		alpha -= 0.75f * Time.deltaTime;

		if(alpha <= 0)
		{
			GoToNextLevel();
		}
	}

	private void GoToNextLevel()
	{
		Application.LoadLevel("MainScene");
	}

	private float RunTimer()
	{
		timer += Time.deltaTime;

		return timer;
	}
}