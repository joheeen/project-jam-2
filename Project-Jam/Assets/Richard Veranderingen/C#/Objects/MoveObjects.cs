﻿using UnityEngine;
using System.Collections;

public class MoveObjects : MonoBehaviour 
{
	public static float movementSpeed;

	[HideInInspector]
	public Vector3 direction;
	[HideInInspector]
	public bool rayHit;

	private void Awake()
	{
		movementSpeed = 100;
		rayHit = false;
	}

	public void Update()
	{
		GetComponent<Rigidbody2D>().velocity = direction * Time.deltaTime * movementSpeed;
	}

	private void ChangePositionAndDirection(float yPos)
	{
		transform.position = new Vector3(transform.position.x, yPos, transform.position.z);
		direction = -direction;
	}

	private void OnTriggerEnter2D(Collider2D col)
	{
		if(!rayHit)
		{
			if(col.gameObject.tag.Equals("Player1"))
			{
				PlayerHealth.player1Hearts--;
			}
			if(col.gameObject.tag.Equals("Player2"))
			{
				PlayerHealth.player2Hearts--;
			}
		}
		if(col.gameObject.tag.Equals("Player") && rayHit)
		{
			if(direction == Vector3.right)
			{
				ChangePositionAndDirection(0.5f);
			}
			else
			{
				ChangePositionAndDirection(-0.5f);
			}
		}
	}
}