﻿using UnityEngine;
using System.Collections;

public class PlayerPushHitCheck : MonoBehaviour 
{
	private RaycastHit2D hit;
	public LayerMask mask;

	private void Update()
	{
		GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
		Debug.DrawRay(transform.position, -transform.up, Color.red);
		hit = Physics2D.Raycast(transform.position, -transform.up, 1, mask);

		if(hit.collider != null)
		{
			if(hit.collider.tag.Equals("PushObject"))
			{
				hit.collider.GetComponent<MoveObjects>().rayHit = true;
				//hit.collider.GetComponent<BoxCollider2D>().enabled = false;
			}
		}
	}
}