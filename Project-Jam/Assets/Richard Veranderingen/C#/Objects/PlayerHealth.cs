﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerHealth : MonoBehaviour 
{
	public Image[] heartsPlayer1;
	public Image[] heartsPlayer2;

	public Sprite textureFull, textureEmpty;

	public static int player1Hearts {get; set;}
	public static int player2Hearts {get; set;}

	public void Awake()
	{
		player1Hearts = 3;
		player2Hearts = 3;
	}

	private void Update()
	{

		Debug.Log(player1Hearts);
		if(player1Hearts == 0 || player2Hearts == 0)
		{
			MoveObjects.movementSpeed = 0;
			SpawnObjects.canSpawn = false;
		}
		SetHearts();
	}

	private void SetHearts()
	{
		switch(player1Hearts)
		{
			case 0:
				heartsPlayer1[0].GetComponent<Image>().sprite = textureEmpty;
				heartsPlayer1[1].GetComponent<Image>().sprite = textureEmpty;
				heartsPlayer1[2].GetComponent<Image>().sprite = textureEmpty;
				break;
			case 1:
				heartsPlayer1[0].GetComponent<Image>().sprite = textureFull;
				heartsPlayer1[1].GetComponent<Image>().sprite = textureEmpty;
				heartsPlayer1[2].GetComponent<Image>().sprite = textureEmpty;
				break;
			case 2:
				heartsPlayer1[0].GetComponent<Image>().sprite = textureFull;
				heartsPlayer1[1].GetComponent<Image>().sprite = textureFull;
				heartsPlayer1[2].GetComponent<Image>().sprite = textureEmpty;
				break;
			case 3:
				heartsPlayer1[0].GetComponent<Image>().sprite = textureFull;
				heartsPlayer1[1].GetComponent<Image>().sprite = textureFull;
				heartsPlayer1[2].GetComponent<Image>().sprite = textureFull;
				break;
		}

		switch(player2Hearts)
		{
			case 0:
				heartsPlayer2[2].GetComponent<Image>().sprite = textureEmpty;
				heartsPlayer2[1].GetComponent<Image>().sprite = textureEmpty;
				heartsPlayer2[0].GetComponent<Image>().sprite = textureEmpty;
				break;
			case 1:
				heartsPlayer2[2].GetComponent<Image>().sprite = textureFull;
				heartsPlayer2[1].GetComponent<Image>().sprite = textureEmpty;
				heartsPlayer2[0].GetComponent<Image>().sprite = textureEmpty;
				break;
			case 2:
				heartsPlayer2[2].GetComponent<Image>().sprite = textureFull;
				heartsPlayer2[1].GetComponent<Image>().sprite = textureFull;
				heartsPlayer2[0].GetComponent<Image>().sprite = textureEmpty;
				break;
			case 3:
				heartsPlayer2[2].GetComponent<Image>().sprite = textureFull;
				heartsPlayer2[1].GetComponent<Image>().sprite = textureFull;
				heartsPlayer2[0].GetComponent<Image>().sprite = textureFull;
				break;
		}
	}
}