﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnObjects : MonoBehaviour 
{
	public Vector3[] spawnPositions;
	public Transform[] objectsToSpawn;
	public float startTime, spawnDelay;
	public float chanceOfPushObject;
	private Transform transformToSpawn;

	public static bool canSpawn{get; set;}

	//Call the method spawnObject every few seconds
	private void Awake()
	{
		transformToSpawn = null;
		canSpawn = true;

		InvokeRepeating("SpawnObject", startTime, spawnDelay);
	}

	//Spawn the objects
	private void SpawnObject()
	{
		if(canSpawn)	
		{
			GiveRandomObject();
			Transform objectPlayer1 = (Transform)Instantiate(transformToSpawn, spawnPositions[0], Quaternion.identity);
			objectPlayer1.GetComponent<MoveObjects>().direction = Vector3.left;

			Transform objectPlayer2 = (Transform)Instantiate(transformToSpawn, spawnPositions[1], Quaternion.identity);
			objectPlayer2.GetComponent<MoveObjects>().direction = Vector3.right;

			Destroy(objectPlayer1.gameObject, 15);
			Destroy(objectPlayer2.gameObject, 15);

			transformToSpawn = null;
		}
	}

	//Generate random number
	//Instantiate object based on the chance to get a push object
	private Transform GiveRandomObject()
	{
		float randomObject = Random.Range(0f, 1f);

		if(randomObject > chanceOfPushObject / 100)
		{
			transformToSpawn = objectsToSpawn[1];
		}
		else
		{
			transformToSpawn = objectsToSpawn[0];
		}


		return transformToSpawn;
	}
}