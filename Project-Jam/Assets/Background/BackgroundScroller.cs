﻿using UnityEngine;
using System.Collections;

public class BackgroundScroller : MonoBehaviour {

	public float startPosition, curPosition, width;
	public float speed;
	RectTransform rectTransform;

	public enum Direction {
		left,
		right
	}
	public Direction direction;


	void Start() {
		rectTransform = this.gameObject.GetComponent<RectTransform>();
		width = rectTransform.sizeDelta.x;
		startPosition = rectTransform.anchoredPosition.x;
		curPosition = rectTransform.anchoredPosition.x;
	}
	
	
	void Update () {
		MovingBackground();
	}
	
	
	void MovingBackground() {

		switch (direction) {
		case Direction.right:
		curPosition -= speed * Time.deltaTime;
		rectTransform.anchoredPosition = new Vector2(curPosition, rectTransform.anchoredPosition.y);
		
			//Reset background position
			if (width * -1 >= rectTransform.anchoredPosition.x)
			{
				curPosition = startPosition;
			}
			break;
		case Direction.left:
			curPosition += speed * Time.deltaTime;
			rectTransform.anchoredPosition = new Vector2(curPosition, rectTransform.anchoredPosition.y);
			
			//Reset background position
			if (width <= rectTransform.anchoredPosition.x)
			{
				curPosition = startPosition;
			}
			break;
		}
	}
}

