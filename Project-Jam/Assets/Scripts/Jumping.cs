﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class Jumping : MonoBehaviour 
{
	public float gravity;

	void Awake ()
	{
		Rigidbody2D rigidBody = GetComponent<Rigidbody2D> ();
		rigidBody.drag = 1;
		rigidBody.gravityScale = gravity;
		rigidBody.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
	}
}
