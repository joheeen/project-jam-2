﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Player : MonoBehaviour {
	
	public enum PlayerOrientation {
		Top,
		Bottom
	}
	
	public PlayerOrientation playerOrientation;
	Rigidbody2D playerRigidbody;
	
	public int jumpForce;
	Vector2 jumpDirection;
	public bool jumping = false;
	
	public Button jumpButton;
	
	// Use this for initialization
	void Start () {
		
		playerRigidbody = GetComponent<Rigidbody2D>();
		jumpButton.onClick.AddListener(() => Jump());
	}
	
	// Update is called once per frame
	public void Jump() {

		if (!jumping) {

			switch (playerOrientation) {
			case PlayerOrientation.Top:
				jumpDirection = new Vector2(0, jumpForce);
				break;
			case PlayerOrientation.Bottom:
				jumpDirection = new Vector2(0, -jumpForce);
				break;
			}
			jumping = true;
			playerRigidbody.AddForce(jumpDirection, ForceMode2D.Impulse);
		}
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "Ground") {
			jumping = false;
		}
	}
}

