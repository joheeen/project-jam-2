﻿using UnityEngine;
using System.Collections;

public class PlayAnimation : MonoBehaviour 
{
	Animator animator;
	public float playAfter;

	void Awake ()
	{
		animator = GetComponent<Animator> ();
		animator.enabled = false;
	}

	void Update ()
	{
		StartAnimation ();
	}

	void StartAnimation ()
	{
		if (playAfter > 0)
			playAfter -= Time.deltaTime;
		else
		{
			playAfter = 0;
			animator.enabled = true;
		}
	}
}
